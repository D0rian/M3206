#!/bin/bash
#HISTFILE=~/.bash_history
#set -o history
 
cat my_history | awk 'BEGIN {FS =";"}''{print $2}' | sort | uniq -c | sort -nr | head -$1

#On change le délimmiteur pour remplacer les espaces par des ; pour les délimiteurs 
