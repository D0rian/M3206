#!/bin/bash

#Test pour savoir si c'est dossier 

[[ -d $1 ]]
if [[ $? -eq 0 ]];then  
	#On renomme notre répertoire
	nom=$(date +%Y_%m_%d_%H%M)_$(basename $1).tar.gz
	#On archive et compresse
	tar cvzf $nom $1 > /dev/null
	echo "creation de l'archive: $nom"
else
	echo "Ce n'est pas un répetoire"
fi 
