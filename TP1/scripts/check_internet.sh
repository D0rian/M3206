#!bin/bash

#Demande de la page de google
echo -e "[...] Checking internet connection [...]"

wget -q --spider www.google.com

#-q : Permet de ne pas afficher la sortie de la cmd dans le terminal
#--spider : Permet de juste faire un check et de ne pas télécharger la page

# Vérification de la réussite ou non de la requête
if [ $? -eq 0 ]; then
	echo -e "[...] Internet acces OK	   [...]" 
else 
	echo -e "[/!\] Not connected to Internet   [/!\]"
	echo -e "[/!\] Please check configuration  [/!\]"
fi
