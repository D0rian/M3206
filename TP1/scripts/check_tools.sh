#!bin/bash

#Liste d'outils à tester
list="git tmux vim thop"
for element in $list
	do
		which $element > /dev/null 2>&1
		if [ $? -eq 0 ];
			then echo -e "[...] $element: installé [...]"
		else
			echo -e "[/!\] $element: pas installé [/!\]"
		fi
	done

