#!/bin/bash

#Vérification du lancement de ssh


if which ssh > /dev/null 2>&1 
then 
	echo "[...] ssh: est installé [...]"
 
	if service ssh status > /dev/null 2>&1
		then echo "[...] ssh: fonctionne [...]"
	else 
  
		echo "[/!\] ssh: le service n'est pas lancé [/!\]"
		echo "[...] ssh: lancement du service 	    [/!\] lancer la commande: '/etc/init.d/ ssh start"
	
	fi

else
	echo "[/!\] ssh: n'est pas installé [/!\] lancer la commande apt-get install openssh-server"

fi

