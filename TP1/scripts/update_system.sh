#!/bin/bash

#Check if we are root, if yes, do the update

if [ $EUID != 0 ];
	then echo -e "[/!\] Vous devez être super-utilisateur [/!\]"
else
	echo -e "[...] update database [...]"
	apt-get update
	echo -e "[...] upgrade system  [...]"
	apt-get upgrade
fi


# L'identifiant du super-utilisateur root est 0, donc dans un premier temps nous vérifions que nous sommes root. Si ce n'est pas le cas on informe l'utilisateur qqu'il doit être super-utilisateur. Lorsque l'on est super-utilisateur on effectue la mise à jour du système.   
